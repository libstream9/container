#ifndef STREAM9_CONTAINER_CONCEPTS_HPP
#define STREAM9_CONTAINER_CONCEPTS_HPP

#include "namespace.hpp"

#include <iterator>
#include <concepts>
#include <ranges>
#include <type_traits>

namespace stream9::container {

namespace detail {

    template<typename I>
    concept three_way_comparable_iterator =
           std::input_iterator<I>
        && ((!std::random_access_iterator<I>)
            || std::three_way_comparable<I, std::strong_ordering> );

} // namespace detail

template<typename T>
struct cpp17_iterator_t {
    using value_type = T;
    using reference = T&;
    using difference_type = std::ptrdiff_t;

    auto operator*() const { return 0; }
    cpp17_iterator_t& operator++() { return *this; }
};

template<typename T>
class cpp17_input_iterator_t : public cpp17_iterator_t<T>
{
public:
    using iterator_category = std::input_iterator_tag;

    T& operator*() const { return *m_ptr; }

    cpp17_input_iterator_t& operator++() { return *this; }
    void operator++(int) { }

    bool operator==(cpp17_input_iterator_t const&) const { return true; }

private:
    T* m_ptr;
};

template<typename X>
concept container = rng::range<X>
    && std::default_initializable<X>
    && std::destructible<X>
    //&& std::copy_constructible<X> //TODO pre-condition
    && std::move_constructible<X>
    && requires (X a, X b) {
           typename X::const_iterator;
           { a.cbegin() } -> std::same_as<typename X::const_iterator>;
           { a.cend() } -> std::same_as<typename X::const_iterator>;
           { a == b } -> std::convertible_to<bool>;
           a.swap(b);
           swap(a, b);
           { a.max_size() } -> std::same_as<rng::range_size_t<X>>;
       }
    //&& detail::three_way_comparable_iterator<rng::iterator_t<X>> //TODO small_vector violates this
    ;

template<typename X>
concept reversible_container = container<X>
    && rng::bidirectional_range<X>
    && requires (X a, X const ca) {
           typename X::reverse_iterator;
           typename X::const_reverse_iterator;
           { a.rbegin() } -> std::same_as<typename X::reverse_iterator>;
           { a.rend() } -> std::same_as<typename X::reverse_iterator>;
           { ca.rbegin() } -> std::same_as<typename X::const_reverse_iterator>;
           { ca.rend() } -> std::same_as<typename X::const_reverse_iterator>;
           { a.crbegin() } -> std::same_as<typename X::const_reverse_iterator>;
           { a.crend() } -> std::same_as<typename X::const_reverse_iterator>;
       };

template<typename X,
         typename T = rng::range_value_t<X> >
concept sequence_container = container<X> //TODO
    && requires (X a, rng::iterator_t<X const> p,
                 cpp17_input_iterator_t<T> i, cpp17_input_iterator_t<T> j)
       {
           a.insert(p, i, j);
       }
    && requires (X a, rng::iterator_t<X const> q1, rng::iterator_t<X const> q2)
       {
           { a.erase(q1, q2) } -> std::same_as<rng::iterator_t<X>>;
       }
    ;

template<typename X,
         typename T = rng::range_value_t<X> >
concept associative_container = container<X> //TODO
    && requires (X a,
                 cpp17_input_iterator_t<T> i, cpp17_input_iterator_t<T> j)
       {
           a.insert(i, j);
       }
    && requires (X a, rng::iterator_t<X const> q1, rng::iterator_t<X const> q2)
       {
           { a.erase(q1, q2) } -> std::same_as<rng::iterator_t<X>>;
       }
    ;

template<typename X, typename I = rng::iterator_t<X const>>
concept has_range_erase =
    requires (X a, I q1, I q2) {
        { a.erase(q1, q2) } -> std::same_as<rng::iterator_t<X>>;
    };

} // namespace stream9::container

#endif // STREAM9_CONTAINER_CONCEPTS_HPP
