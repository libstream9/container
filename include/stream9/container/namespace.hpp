#ifndef STREAM9_CONTAINER_NAMESPACE_HPP
#define STREAM9_CONTAINER_NAMESPACE_HPP

namespace std::ranges {}
namespace stream9::ranges {}

namespace stream9::container {

namespace rng { using namespace std::ranges; }
namespace rng { using namespace stream9::ranges; }
namespace con { using namespace container; }

} // namespace stream9::ranges

#endif // STREAM9_CONTAINER_NAMESPACE_HPP
