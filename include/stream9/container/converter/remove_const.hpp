#ifndef STREAM9_CONTAINER_CONVERTER_REMOVE_CONST_HPP
#define STREAM9_CONTAINER_CONVERTER_REMOVE_CONST_HPP

#include "../concepts.hpp"
#include "../namespace.hpp"

#include <iterator>
#include <ranges>
#include <type_traits>

namespace stream9::container {

namespace remove_const_ {

    struct api
    {
        template<rng::range C>
            requires (!std::is_const_v<C>)
                  && has_range_erase<C>
        constexpr rng::iterator_t<C>
        operator()(C& c, rng::iterator_t<C const> const it) const
            noexcept(noexcept(c.erase(it, it)))
        {
            return c.erase(it, it);
        }
    };

} // namespace remove_const_

inline constexpr remove_const_::api remove_const;

} // namespace stream9::container

#endif // STREAM9_CONTAINER_CONVERTER_REMOVE_CONST_HPP
