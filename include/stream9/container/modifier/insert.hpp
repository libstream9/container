#ifndef STREAM9_CONTAINER_MODIFIER_INSERT_HPP
#define STREAM9_CONTAINER_MODIFIER_INSERT_HPP

#include "../concepts.hpp"
#include "../namespace.hpp"

#include <concepts>
#include <iterator>
#include <ranges>
#include <type_traits>

namespace stream9::container {

namespace insert_ {

    template<typename T>
    concept has_reserve = requires (T& t, rng::range_size_t<T> n) {
        t.reserve(n);
    };

    template<sequence_container C>
    auto to_iterator(C& c, rng::iterator_t<C const> it) //TODO check out std implementation
    {
        return c.erase(it, it);
    }

    template<typename> struct value_type;

    template<rng::range T>
    struct value_type<T>
    {
        using type = rng::range_value_t<T>;
    };

    template<std::input_iterator T>
    struct value_type<T>
    {
        using type = std::iter_value_t<T>;
    };

    template<typename T>
    using value_type_t = value_type<T>::type;

    template<typename, typename>
    struct is_same_value : std::false_type {};

    template<typename T1, typename T2>
        requires std::same_as<value_type_t<T1>, value_type_t<T2>>
    struct is_same_value<T1, T2> : std::true_type {};

    template<typename T1, typename T2>
    inline constexpr bool is_same_value_v = is_same_value<T1, T2>::value;

    template<sequence_container T, typename I, typename S>
        requires (!std::convertible_to<S, I>)
    rng::iterator_t<T>
    seq_insert(T& c, rng::iterator_t<T const> pos, I first, S last)
    {
        if (first == last) return to_iterator(c, pos);

        if constexpr (std::sized_sentinel_for<S, I> && has_reserve<T>) {
            c.reserve(c.size() + (last - first));
        }

        auto it = first;
        pos = c.insert(pos, *it);
        ++it;

        for (; it != last; ++it) {
            pos = c.insert(std::next(pos), *it);
        }

        return to_iterator(c, pos);
    }

    template<sequence_container T, typename I, typename S>
        requires std::convertible_to<S, I>
    rng::iterator_t<T>
    seq_insert(T& c, rng::iterator_t<T const> pos, I first, S last)
    {
        return c.insert(pos, first, last);
    }

    template<associative_container C, typename I, typename S>
        requires (!std::convertible_to<S, I>)
    void
    assoc_insert(C& c, I const first, S const last)
    {
        if constexpr (std::sized_sentinel_for<S, I> && has_reserve<C>) {
            c.reserve(c.size() + (last - first)); //TODO reserve_if_possible
        }

        for (auto it = first; it != last; ++it) {
            c.insert(*it);
        }
    }

    template<associative_container C, typename I, typename S>
        requires std::convertible_to<S, I>
    void
    assoc_insert(C& c, I const first, S const last)
    {
        c.insert(first, last);
    }

    struct api {

        template<sequence_container C, rng::input_range R>
            requires is_same_value_v<C, R>
        rng::iterator_t<C>
        operator()(C& c, rng::iterator_t<C const> pos, R const& r) const
        {
            return (seq_insert)(c, pos, rng::begin(r), rng::end(r));
        }

        template<sequence_container C,
                 std::input_iterator I, std::sentinel_for<I> S >
            requires is_same_value_v<C, I>
        rng::iterator_t<C>
        operator()(C& c, rng::iterator_t<C const> pos, I first, S last) const
        {
            return (seq_insert)(c, pos, first, last);
        }

        template<associative_container C, rng::input_range R>
            requires is_same_value_v<C, R>
        void
        operator()(C& c, R const& r) const
        {
            (assoc_insert)(c, rng::begin(r), rng::end(r));
        }

        template<associative_container C,
                 std::input_iterator I, std::sentinel_for<I> S >
            requires is_same_value_v<C, I>
        void
        operator()(C& c, I first, S last) const
        {
            (assoc_insert)(c, first, last);
        }

    };

} // namespace insert_

inline constexpr insert_::api insert;

} // namespace stream9::container

#endif // STREAM9_CONTAINER_MODIFIER_INSERT_HPP
