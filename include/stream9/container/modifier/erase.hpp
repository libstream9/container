#ifndef STREAM9_CONTAINER_MODIFIER_ERASE_HPP
#define STREAM9_CONTAINER_MODIFIER_ERASE_HPP

#include "../concepts.hpp"
#include "../namespace.hpp"

#include <concepts>
#include <iterator>
#include <ranges>

namespace stream9::container {

namespace erase_ {

    struct api {

        template<rng::forward_range C, rng::input_range R>
            requires has_range_erase<C>
                  && std::convertible_to<rng::iterator_t<R>, rng::iterator_t<C const>>
        constexpr rng::iterator_t<C>
        operator()(C& c, R const& r) const
        {
            return c.erase(rng::begin(r), rng::end(r));
        }

    };

} // namespace erase_

inline constexpr erase_::api erase;

} // namespace stream9::container

#endif // STREAM9_CONTAINER_MODIFIER_ERASE_HPP
