#ifndef STREAM9_CONTAINER_MODIFIER_ERASE_IF_HPP
#define STREAM9_CONTAINER_MODIFIER_ERASE_IF_HPP

#include "../concepts.hpp"
#include "../namespace.hpp"

#include <algorithm>
#include <concepts>
#include <functional>
#include <iterator>
#include <ranges>

namespace stream9::container {

namespace erase_if_ {

    template<typename Proj, typename I>
    concept projection =
       std::indirectly_regular_unary_invocable<Proj, I>;

    template<typename Pred, typename Proj, typename I>
    concept predicate_on_projection =
        std::indirect_unary_predicate<Pred, std::projected<I, Proj>>;

    template<typename Pred, typename Proj>
    struct projected_predicate
    {
        Pred* pred;
        Proj* proj;

        auto operator()(auto&& i) const
        {
            return (*pred)((*proj)(i));
        }

        projected_predicate(Pred pred_, Proj proj_)
            : pred { &pred_ }
            , proj { &proj_ }
        {}
    };

    template<typename C, typename Pred, typename Proj>
    concept has_adl_erase_if =
        requires (C&& r, projected_predicate<Pred, Proj> pred) {
            { erase_if(r, pred) } -> std::same_as<rng::range_size_t<C>>;
        };

    struct api {

        template<rng::forward_range C, typename Pred, typename Proj = std::identity>
            requires (!has_adl_erase_if<C, Pred, Proj>)
                  && has_range_erase<C>
                  && std::permutable<rng::iterator_t<C>>
                  && projection<Proj, rng::iterator_t<C>>
                  && predicate_on_projection<Pred, Proj, rng::iterator_t<C>>
        constexpr rng::range_size_t<C>
        operator()(C& c, Pred pred, Proj proj = {}) const
        {
            auto const removed = rng::remove_if(c, pred, proj);
            c.erase(removed.begin(), removed.end());

            return rng::size(removed);
        }

        template<rng::forward_range C, typename Pred, typename Proj = std::identity>
            requires has_adl_erase_if<C, Pred, Proj>
        constexpr rng::range_size_t<C>
        operator()(C& c, Pred pred, Proj proj = {}) const
        {
            return erase_if(c, projected_predicate(pred, proj));
        }

    };

} // namespace erase_if_

inline constexpr erase_if_::api erase_if;

} // namespace stream9::container

#endif // STREAM9_CONTAINER_MODIFIER_ERASE_IF_HPP
